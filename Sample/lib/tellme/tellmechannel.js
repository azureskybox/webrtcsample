/**
 * written by jphong.
 */
function TellmeChannel() {
	
	var DEBUG = TellmeChannel_DEBUG;
	
	var xhr = null;
	var _channel = null;

	/**
	 * Google channer server : http://tellmech2.appspot.com/channeltest
	 */
	var channelServerGoogle = TellmeChannel_ChannelServer;
	
	/**
	 * WebSocket server : ws://211.110.5.180:8080
	 */
	var channelServerWebSocket = TellmeChannel_WebSocketServer;

	/**
	 * WebSocket Callbacks : WebSocket 시 사용하는 콜백  
	 */
	var onopencallback, onmessagecallback, onerrorcallback, onclosecallback;
	
	/**
	 * 사용자가 channel 접속 시 사용하는 id를 설정 (자신의 유니크한 키값) 
	 */
	var _userId = null;

	/**
	 * 사용하는 채널 타입  
	 */
	var _channelType = null;
	
	/**
	 * 채널 타입의 const   
	 */
	var CHANNEL_GOOGLE = "CHANNEL_GOOGLE";
	var CHANNEL_WEBSOCKET = "CHANNEL_WEBSOCKET";
	var CHANNEL_SOCKETIO = "CHANNEL_SOCKETIO";
	
	var loopObj = null;

	var SIG_ACTION = {"DONE":10, "CALL":11, "RETURN":12, "RCALL":13, "ACK":14};
	var SIG_TYPE = {
			"NONE":100,
			//WebRTC getUserMedia 
			"GUM":101,
			//상대편에게 전화 연결 요청
			"REQUESTCALL":102,
			//WebRTC 연결 상태
			"CONNECT" : 103,
			//WebRTC 연결종료 상태
			"DISCONNECT" : 104,
			//질의응답 클래스 시작
			"CLASSSTART" : 105,
			//질의응답 클래스 종료
			"CLASSEND" : 106
	};
	
	/*****************************
	 * 
	 * PUBLIC PROPERTIES
	 * 
	 *****************************/
	/**
	 * TellmeChannel을 초기화하는 함수
	 * 객체 생성 후 정상적으로 TellmeChannel을 사용하기 위해서 반드시 호출해야 함
	 * - Parameter
	 *  type(String) 		: google channel(CHANNEL_WEBSOCKET) 혹은 websocket(CHANNEL_WEBSOCKET) 중 선택 가능
	 *  onopen(Function) 	: 각 channel에 대한 open callback
	 *  onmessage(Function) : 각 channel에 대한 message callback
	 *  onerror(Function) 	: 각 channel에 대한 error callback
	 *  onclose(Function)	: 각 channel에 대한 close callback
	 *  userId(String) 		: 서버접속 시 사용하는 id
	 */
	function chInitChannel(type, onopen, onmessage, onerror, onclose, userId) {
		dlog("chInitChannel was called - tellmechannel.js_v."+ TELLME_CHANNEL_VERSION );
		dlog("channelType:"+type+", userId:"+userId);
		
		/**
		 * 클라이언트의 id를 세팅한다.    
		 */
		_userId = userId;
		
		/**
		 * WebSocket의 callback을 세팅한다. 
		 */
		onopencallback = onopen;
		onmessagecallback = onmessage;
		onerrorcallback = onerror;
		onclosecallback = onclose;
		
		if(type !== null) {
			_channelType = CHANNEL_SOCKETIO;
		}
		else {
			_channelType = CHANNEL_WEBSOCKET;
		}
		
		if(_channelType === CHANNEL_WEBSOCKET || _channelType === CHANNEL_SOCKETIO) {
			chCreateChannel();
		}
		else if(_channelType === CHANNEL_GOOGLE) {
			var message = chCreateMessage(userId, "", "REG", "", userId);
			chSendMessage(message);
		}
	}
	
	/**
	 * channel을 생성한다. 
	 * google channel을 사용할 경우 onServerResponse()에서 얻어온 value를 token으로 등록한다. 
	 */
	function chCreateChannel(token) {
		dlog("chCreateChannel was called");
		
		if(_channelType === CHANNEL_GOOGLE) {
			_channel = new goog.appengine.Channel(token);
			_channel = _channel.open();
		}
		else if(_channelType === CHANNEL_WEBSOCKET) {
			 //WebSocket을 사용할 경우 현재는 다음의 URL을 사용한다. 
	        _channel = new WebSocket(channelServerWebSocket);
		}
		else if(_channelType === CHANNEL_SOCKETIO) {
			//TODO socket.io 서버주소 상수 선언 필요
			_channel = io.connect(TellmeChannel_WebSocketIOServer);
		}
		
		//socket.io를 사용하는 경우, 콜백 함수를 직접 해당 객체에 설정하지 않는다.
		if(_channelType === CHANNEL_SOCKETIO) {
			_channel.on('open', onopencallback);
			_channel.on('message', onmessagecallback);
			
			//DEBUG용 메세지 콜백 추가
			if(_userId == "debugger") {
				_channel.on('json', onmessagecallback);
			}
			
		}
		//채널의 callback함수를 chInitChannel시에 client로 부터 요청받은 콜백으로 등록한다. 
		else {
		   _channel.onopen = onopencallback;
		   _channel.onmessage = onmessagecallback;
		   _channel.onerror = onerrorcallback;
		   _channel.onclose = onclosecallback;
		}
	}
	
	/**
	 * 현재 Open 되어 있는 채널을 닫는다. 
	 */
	function chCloseChannel() {
		dlog("chCloseChannel was called");
		clearInterval(loopObj);
		
		if(_channelType === CHANNEL_GOOGLE) {
			_channel.close();
		}
		else if(_channelType === CHANNEL_WEBSOCKET) {
			channel.close();
		}
		else if(_channelType === CHANNEL_SOCKETIO) {
		}
	}
	
	/**
	 * TellmeChannel 로 송신하기위한 JSON 객체를 생성하여 리턴한다. 
     * - Parameter
	 *  receiver(String): Channel 서버에 등록되어 있는 수신자의 ID
	 *  action(String): SEND, REG 와 같은 Sever에 요청하는 특정 동작
	 *  type(String): ACTION에 대한 세부적인 설정값 
	 *  message(String) : Server에 전달하는 특정 메세지
	 */
	function chCreateMessage(receiver, action, type, message) {
		dlog("chCreateMessage was called");
		var request = {};
		request.SENDER = _userId;
		request.RECEIVER = receiver;
		request.ACTION = action;
		request.TYPE = type;
		request.MESSAGE = message;
		
		return request;
	}
	
	function chChangeUserId(userId) {
		_userId = userId;
	}
	
	/**
	 *  자신의 _userID를 Channel 서버에 등록한다. 
     * - Parameter
	 *  type(String): 사용되는 Channel의 타입  
	 */
	function chRegistClient(type, createRoom) {
		dlog("chRegistClient was called");
		//Google Channel API를 사용할 경우 등록할 필요는 없다. 
		if(_channelType === CHANNEL_GOOGLE)
			return;
		
		//Websocket 서버를 사용한다면 socket연결이 끊어지지 않기 위하여 ACK을 주기적으로 보낸다. 
		if(_channelType === CHANNEL_WEBSOCKET) {
			loopObj = setInterval(function() {
				chSendSignal("", SIG_ACTION.ACK, SIG_TYPE.NONE);
			}, 60*1000);
		}
		
		console.log("chRegistClient was called, clientId:"+_userId);
		
		//사용자 등록 시 해당 사용자가 방장임을 확인]
		var regMsg = {};
		regMsg.CREATE_ROOM = createRoom;
		
		if(type == null)
			var message = chCreateMessage("", "REG", "", regMsg);
		else
			var message = chCreateMessage("", "REG", type, regMsg);  //Debug용 
		
		chSendMessage(message);
	}
	
	function chUnRegistClient() {
		dlog("chUnRegistClient was called");
		
		if(_channelType === CHANNEL_WEBSOCKET && loopObj != null) {
			clearInterval(loobObj);
			loopObj = null;
		}
		
		var regMsg = {};
		
		var message = chCreateMessage("", "UNREG", "", regMsg);  //Debug용 
		chSendMessage(message);
	}
	
	/**
	 * JSON객체의 메제시를 Channel 서버에 전송한다. 
	 */
	function chSendMessage(message) {
		dlog("chSendMessage was called");
		
		//JSON객채를 String화 한다. 
		var strMessage = JSON.stringify(message);
		
		if(_channelType === CHANNEL_GOOGLE) {
			sendRequest("msg="+strMessage);
		}
		else if(_channelType === CHANNEL_WEBSOCKET) {
			if(_channel)
				_channel.send(strMessage);
		}
		else if(_channelType === CHANNEL_SOCKETIO) {
			if(_channel)
				_channel.emit('message', strMessage);
		}
	}

	/**
	 *  Signal 메세지를 Channel 서버에 전송한다. 
     * - Parameter
	 *  receiver(String): Channel 서버에 등록되어 있는 수신자의 ID
	 *  sigAction(String): SIG_ACTION = {"DONE":10, "CALL":11, "RETURN":12, "RCALL":13, "ACK":14};
	 *  sigType(String): 	var SIG_TYPE = {
												"NONE":100,
												//WebRTC getUserMedia 
												"GUM":101,
												//상대편에게 전화 연결 요청
												"REQUESTCALL":102,
												//WebRTC 연결 상태
												"CONNECT" : 103,
												//WebRTC 연결종료 상태
												"DISCONNECT" : 104
												};
	 *  sigPre(String) : Server에 전달하는 특정 메세지
	 */
	function chSendSignal(receiver, sigAction, sigType, sigPre) {
		dlog("chSendSignal was called");
		
		var fullSignal = (sigAction*1000000) + (sigType*1000);
		if(sigPre != null) {
			fullSignal += sigPre;
		}
		
		//JSON 메세지 객체를 생성하고 
		var message = chCreateMessage(receiver, "SEND", "SIG", ""+fullSignal);
		//message를 Channel 서버에 전송한다. 
		chSendMessage(message);
	}
	
	/**
	 * 특정 시그널메세지를 Channel 서버에 전송한다. 채널 서버에서는 exReceiver를 제외한 모든 client에게 메세지를 전송한다.
	 *  receiver(String): 사용되지 않는다. 
	 *  sigAction(String) : {"DONE":10, "CALL":11, "RETURN":12, "RCALL":13, "ACK":14};
	 *  sigType(String): 	var SIG_TYPE = {
												"NONE":100,
												//WebRTC getUserMedia 
												"GUM":101,
												//상대편에게 전화 연결 요청
												"REQUESTCALL":102,
												//WebRTC 연결 상태
												"CONNECT" : 103,
												//WebRTC 연결종료 상태
												"DISCONNECT" : 104
												};
	 *  sigPre(String) : Server에 전달하는 특정 메세지
	 *  exReceiver(Array) : 이 브로드캐스팅 메세지를 받지 않을 client id의 array 
	 */
	function chBroadcastSignal(receiver, sigAction, sigType, sigPre, exReceiver) {
		dlog("chSendBroadcast was called");
		
		var brcastMsg = {};
		brcastMsg.SIG_ACTION = sigAction;
		brcastMsg.SIG_TYPE = sigType;
		brcastMsg.SIG_PRE = sigPre;
		brcastMsg.RECEIVER = receiver;
		brcastMsg.EXRECEIVER = exReceiver;
		
		dlog("JSON broadcast message:" + JSON.stringify(brcastMsg));
		
		var brcastRequest = chCreateMessage("", "BRSEND", "SIG", JSON.stringify(brcastMsg));
		chSendMessage(brcastRequest);
	}
	
	/**
	 * 특정 메세지를 Channel 서버에 전송한다. 채널 서버에서는 exReceiver를 제외한 모든 client에게 메세지를 전송한다.
	 *  receiver(String): 사용되지 않는다. 
	 *  message(String) : 특정 Client에 전송할 메세지 
	 *  exReceiver(Array) : 이 브로드캐스팅 메세지를 받지 않을 client id의 array 
	 */
	function chBroadcastMessage(receiver, message, exReceiver) {
		dlog("chBroadcastMessage was called");
		
		var brcastMsg = {};
		brcastMsg.MESSAGE = message;
		brcastMsg.RECEIVER = receiver;
		brcastMsg.EXRECEIVER = exReceiver;
		
		dlog("JSON broadcast message:" + JSON.stringify(brcastMsg));
		
		var brcastRequest = chCreateMessage("", "BRSEND", "MSG", JSON.stringify(brcastMsg));
		chSendMessage(brcastRequest);
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * 
	 *****************************/
	/**
	 * 구글 채널 서버에 request를 전송한다. 
	 */
	function sendRequest(request) {
		xhr = new XMLHttpRequest();
		xhr.onreadystatechange = onServerResponse;
		xhr.open("POST", channelServerGoogle, true);
		
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    xhr.send(request);
	}

	/**
	 * 구글 채널 서버에 request를 전송후 응답받는 callback.
	 * google channel token을 전송 받는다. 
	 */
	function onServerResponse() {
		if(xhr.readyState == 4) {
			if(xhr.status == 200) {
				var str = xhr.responseText;
				chCreateChannel(str);
			}
		}
	}
	
	/**
	 * 특정 메세지를 loging 한다. 
	 */
	function dlog(message) {
		if(DEBUG)
			console.log(message);
	}
	
	/*****************************
	 * 
	 * Wrapping methods for TellmeRTC 
	 * 
	 *****************************/
	/**
	 * Receiver에게 WebRTC 통신을 요청을 전송한다.
	 * Tellme 에서는 강사가 사용하는 함수.  
	 */
	function tellmeRequestCall(receiver) {
		//SIG_ACTION.CALL 을 사용하여 특정 receiver한테 요청이 간다. 
		//SIG_TYPE.REQUESTCALL 을 사용하여 WebRTC 요청 type임을 알린다. 
		chSendSignal(receiver, SIG_ACTION.CALL, SIG_TYPE.REQUESTCALL);
	}
	
	/**
	 * Receiver에게 WebRTC 통신을 요청에 대한 응답을 전송한다. 
	 * Tellme 에서는 수강자가 사용하는 함수.  
	 */
	function tellmeReceiveCall(receiver) {
		//SIG_ACTION.RETURN 을 사용하여 특정 receiver한테 응답 결과 유무를 알린다. 
		//SIG_TYPE.REQUESTCALL 을 사용하여 WebRTC 요청 type임을 알린다. 
		//TODO: 20131204 chisu 확인 필요! 
		chSendSignal(receiver, SIG_ACTION.RETURN, SIG_TYPE.REQUESTCALL);
	}
	
	/**
	 * Receiver에게 WebRTC 의 getUserMedia() 함수 사용을 요청한다.
	 * Tellme 에서는 강사가 사용하는 함수.   
	 */
	function tellmeRequestGum(receiver) {
		//SIG_ACTION.CALL 을 사용하여 특정 receiver한테 요청이 간다. 
		//SIG_TYPE.GUM 을 사용하여 WebRTC getUserMedia 사용 요청 type임을 알린다. 
		chSendSignal(receiver, SIG_ACTION.CALL, SIG_TYPE.GUM);
	}
	
	/**
	 * Receiver에게 WebRTC 의 getUserMedia() 함수를 정상적으로 사용하였음을 알린다. 
	 * (signaling이 준비 되었음을 알린다.) 
	 * Tellme 에서는 강사, 수강자가 사용하는 함수.   
	 */
	function tellmeSendGumCompleted(receiver, isCaller) {
		if(isCaller) {
			//Caller인 경우에는 Channel 서버에 응답을 SIG_ACTION.DONE 으로 한다. 
			chSendSignal(receiver, SIG_ACTION.DONE, SIG_TYPE.GUM);
		}
		else {
			//Callee인 경우에는 Channel 서버에 응답을 SIG_ACTION.RETURN 으로 한다. 
			//이와 같이 함은 caller, callee 둘중 순서에 상관없이 연결을 보장하기 위함이다
			chSendSignal(receiver, SIG_ACTION.RETURN, SIG_TYPE.GUM, SIG_TYPE.GUM);
		}
	}
	
	/**
	 * exReceiverIdList를 제외한 Channel Server에 등록된 모든 Client에게 connect status를 브로드캐스팅한다. 
	 * Tellme에서 강사가 사용하는 함수.
	 * -parameter
	 * receiver(String): receiver 를 포함하는 id에게 브로드캐스팅을 한다. 
	 * example: receiver 가 1234 라면 12343333에게는 브로드캐스팅을 한다. 
	 *                                       23453333에게는 브로드 캐스팅을 하지 않는다. 
	 * exReceiverIdList(Array) : 브로드 캐스팅을 제외하는 client Id 배열 
	 * 								Tellme에서는 강사와 질문 등록자가 예외사항이 된다. 
	 */
	function tellmeBroadcastConnState(receiver, isConnected, exReceiverIdList) {
		var sigType = null;
		
		if(isConnected)
			sigType = SIG_TYPE.CONNECT;
		else
			sigType = SIG_TYPE.DISCONNECT;
		
		if(exReceiverIdList != null)
			chBroadcastSignal(receiver, SIG_ACTION.CALL, sigType, "", exReceiverIdList);
		else 
			chBroadcastSignal(receiver, SIG_ACTION.CALL, sigType, "");
	}
	
	function tellmeBroadcastClassState(receiver, isStarted, exReceiverIdList) {
		var sigType = null;
		
		if(isStarted)
			sigType = SIG_TYPE.CLASSSTART;
		else
			sigType = SIG_TYPE.CLASSEND;
		
		if(exReceiverIdList != null)
			chBroadcastSignal(receiver, SIG_ACTION.CALL, sigType, "", exReceiverIdList);
		else 
			chBroadcastSignal(receiver, SIG_ACTION.CALL, sigType, "");
	}
	
	/**
	 * exReceiverIdList를 제외한 Channel Server에 등록된 모든 Client에게 connect status를 브로드캐스팅한다. 
	 * Tellme에서 강사가 사용하는 함수.
	 * -parameter
	 * receiver(String): receiver 를 포함하는 id에게 브로드캐스팅을 한다. 
	 * example: receiver 가 1234 라면 12343333에게는 브로드캐스팅을 한다. 
	 *                                       23453333에게는 브로드 캐스팅을 하지 않는다. 
	 * message(String): 브로드캐스팅할 특정 메세지. 
	 * exReceiverIdList(Array) : 브로드 캐스팅을 제외하는 client Id 배열 
	 * 								Tellme에서는 강사와 질문 등록자가 예외사항이 된다. 
	 */
	function tellmeBroadcastMessage(receiver, message, exReceiveIdList) {
		chBroadcastMessage(receiver, message, exReceiveIdList);
	}
	
	/**
	 * 현재 특정 클래스가 시작 되었는지 혹은 종료 되었는지 설정한다.
	 * - Parameter
	 * isStart(Boolean) : true이면 클래스가 시작 되었음을 의미한다.
	 * classId(String) : 상태값을 변경 할 클래스의 id
	 * 
	 */
	function tellmeSetClassState(isStart, classId, broadcast) {
		var param = {};
		param.roomid = classId;
		param.type = "CLASS_STATE";
		param.noti = broadcast;
		
		if(isStart)
			param.val = "STATE_START";
		else
			param.val = "STATE_CLOSED";
		
		var setRequest = chCreateMessage("", "REQUEST", "SET", JSON.stringify(param));
		chSendMessage(setRequest);
	}
	
	/**
	 * 특정 클래스가 현재 시작 되었는지 종료 되었는지 확인한다.
	 * - Paramter
	 * classId(String) : 상태값을 확인 할 클래스의 id
	 */
	function tellmeGetClassState(classId) {
		var param = {};
		param.roomid = classId;
		param.type = "CLASS_STATE";
		
		var setRequest = chCreateMessage("", "REQUEST", "GET", JSON.stringify(param));
		chSendMessage(setRequest);
	}
	
	function tellmeClassUserNum(classId) {
		var param = {};
		param.roomid = classId;
		param.type = "CLASS_USERS";
		
		
		var setRequest = chCreateMessage("", "REQUEST", "GET", JSON.stringify(param));
		chSendMessage(setRequest);
	}
	
	function tellmeSetUserConnState(roomId, userId, isConnected, noti) {
		var param = {};
		param.roomid = roomId;
		param.userid = userId;
		param.type = "USER_STATE";
		param.noti = noti;
		
		if(isConnected) 
			param.val = "STATE_IN_CALL";
		else
			param.val = "STATE_IN_WAITING";
		
		var setRequest = chCreateMessage("", "REQUEST", "SET", JSON.stringify(param));
		chSendMessage(setRequest);
	}
	
	/**
	 * Caller라면 Callee에게 Callee라면 Caller에게 연결이 종료되었음을 알린다. 
	 */
	function tellmeDisconnectCall() {
		var byeRequest = chCreateMessage("", "BYE", "", "");
		chSendMessage(byeRequest);
	}
	
	return {
		CHANNEL_GOOGLE : CHANNEL_GOOGLE,
		CHANNEL_WEBSOCKET : CHANNEL_WEBSOCKET,
		CHANNEL_SOCKETIO : CHANNEL_SOCKETIO,
		SIG_ACTION : SIG_ACTION,
		SIG_TYPE : SIG_TYPE,
		chInitChannel : chInitChannel,
		chCloseChannel : chCloseChannel,
		chCreateChannel : chCreateChannel,
		chCreateMessage : chCreateMessage,
		chRegistClient : chRegistClient,
		chChangeUserId : chChangeUserId,
		chUnRegistClient : chUnRegistClient,
		chSendMessage : chSendMessage,
		chSendSignal : chSendSignal,
		chBroadcastSignal : chBroadcastSignal,
		chBroadcastMessage : chBroadcastMessage,
		tellmeRequestCall: tellmeRequestCall,
		tellmeReceiveCall : tellmeReceiveCall,
		tellmeRequestGum : tellmeRequestGum,
		tellmeSendGumCompleted : tellmeSendGumCompleted,
		tellmeBroadcastConnState : tellmeBroadcastConnState,
		tellmeBroadcastMessage : tellmeBroadcastMessage,
		tellmeBroadcastClassState : tellmeBroadcastClassState,
		tellmeSetClassState : tellmeSetClassState,
		tellmeGetClassState : tellmeGetClassState,
		tellmeClassUserNum : tellmeClassUserNum,
		tellmeSetUserConnState : tellmeSetUserConnState,
		tellmeDisconnectCall: tellmeDisconnectCall
 	};
}
