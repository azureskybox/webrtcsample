/**
 * 
 */
var TELLME_RTC_VERSION = "0.40.13";
var TELLME_CHANNEL_VERSION = "0.30.22";
var TELLME_TEST_VERSION = "0.30.28";

/*****************************
 * 
 * About WebRTC Settings
 * 
 *****************************/
var pcConfig = {"iceServers": [
                {"url": "stun:stun.l.google.com:19302"} 
				//{"url":"stun:stun1.l.google.com:19302"},
				//{"url":"turn:211.110.5.180:3479?transport=udp","credential":"0xb57bdab6de4968eefb2a495239658847","username":"tellme"},
				//{"url":"turn:211.110.5.180:3479?transport=tcp","credential":"0xb57bdab6de4968eefb2a495239658847","username":"tellme"}
				//{"url":"turn:211.110.5.180:3479?transport=udp","credential":"0xe9c0595d111b1cf11a106fef106b9b63","username":"tellme2"},
				//{"url":"turn:211.110.5.180:3479?transport=tcp","credential":"0xe9c0595d111b1cf11a106fef106b9b63","username":"tellme2"}
]};

//Nable STUN Server 
var pcConfigCaller = {"iceServers":[{"url":"turn:test@1.237.187.34:3478","credential":"1234"}]} ;
var pcConfigCallee = {"iceServers":[{"url":"turn:test@1.237.187.34:3478","credential":"1234"}]} ;

//anyfirewall STUN Server
/*
var pcConfigCaller = {"iceServers": [
                               {"url":"turn:turn.anyfirewall.com:3478?transport=udp","credential":"tellmecaller","username":"tellme_caller"},
                               {"url":"turn:turn.anyfirewall.com:3478?transport=tcp","credential":"tellmecaller","username":"tellme_caller"},
]};
var pcConfigCallee = {"iceServers": [
                               {"url":"turn:turn.anyfirewall.com:3478?transport=udp","credential":"tellmecallee","username":"tellme_callee"},
                               {"url":"turn:turn.anyfirewall.com:3478?transport=tcp","credential":"tellmecallee","username":"tellme_callee"},
]};
*/

var pcConstraints = {"optional": [{"DtlsSrtpKeyAgreement": true}]};
var offerConstraints = {"optional": [], "mandatory": {}};
var mediaConstraints = {"audio": true, "video": false};

//for test
var callerMediaConst = {"audio": true, "video": true};
var calleeMediaConst = {"audio": true, "video": true};

var sdpConstraints = {'mandatory': {
    'OfferToReceiveAudio': true,
    'OfferToReceiveVideo': true }};

//for test
var calleeSdpConstraints = {'mandatory': {
    'OfferToReceiveAudio': true,
    'OfferToReceiveVideo': true }};

var stereo = false;
var audio_send_codec = '';
var audio_receive_codec = 'opus/48000';

/*****************************
 * 
 * About TellmeChannel
 * 
 *****************************/
var TellmeChannel_DEBUG = true;
var TellmeChannel_WebSocketServer = "ws://211.110.5.180:8080";
var TellmeChannel_WebSocketIOServer = "http://211.110.5.180:8080";
var TellmeChannel_ChannelServer = "http://tellmech2.appspot.com/channeltest";

/*****************************
 * 
 * About SKT WebRTC
 * 
 *****************************/
//RESTful API url pattern : http://{serverRoot}/{apiVersion}/RCS/{RCSInstanceId}/{Service}/{ServiceInstanceId}
var SERVER_SCHEME = "http";
var SERVER_PORT = "8070";
var SERVER_DOMAIN = "1.237.187.34";
var SERVER_ADDRESS = SERVER_SCHEME + "://" + SERVER_DOMAIN + (SERVER_PORT=""?"":":"+SERVER_PORT);

var VERSION = "1.0";
var RCS = "RCS";
var API_PREFIX = VERSION + "/" + RCS;

SERVER_ADDRESS += "/" + API_PREFIX;

var SERVICE = "VideoShare";

//request body properties
var RESPONSE_BODY_PROPERTIES = {
	"USER_ID" : "userid",
	"USER_PASSWORD" : "userpwd"
};

//response body 
var RESPONSE_BODY_PROPERTIES = {
	"OUATH_TOKEN" : "ouath_token",
	"EXPIRE_TIME" : "expire_time",
	"INSTANCE_ID" : "instance_id"
};
