/**
 * written by jphong.
 */
function TellmeRTC() {
	var DEBUG = true;
	var TEST = false;
	var TESTS = {
		//HOST Candidate만 사용
		"HOST_CONNECTION" : true,
		//Amazon MayDay 기술 확인
		"ONE_SIDE_COMM" : true
	};
	
	//TellmeRTC Constants
	var CALLER = 1;
	var CALLEE = 0;
	
	var WEBRTC_STEP_NONE = 100;
	var WEBRTC_STEP_INIT = 101;
	var WEBRTC_STEP_GUM_SUCCESS = 102;
	var WEBRTC_STEP_GUM_FAILED = 103;
	var WEBRTC_STEP_SIGNALING = 104;
	var WEBRTC_STEP_CONNECTED = 105;
	var WEBRTC_STEP_DISCONNECTED = 106;
	
	/*****************************
	 * 
	 * CALLBACK PROPERTIES
	 * 
	 *****************************/
	//TellmeRTC status변화에 대한 callback
	var ontmrtcstateCallback = null;
	
	//TellmeRTC가 signaling 시 메세지를 주고받기 위해 호출하는 callback
	var ontmrtcsendmessage = null;
	var ontmrtcrecvmessage = null;
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * 
	 *****************************/
	/**
	 * CALLEE or CALLER로 사용자의 역할을 설정할 수 있다.
	 */
	var _userRole = null;
	
	/**
	 * channel시 사용되는 id이다.
	 */
	var _userId = null;
	var _imsId = null;
	
	/**
	 * signaling시 메세지를 수신하는 사람의 id이다.
	 * 현재 연결을 시도하고 있는 peer를 유일하게 식별할 수 있는 값으로 설정한다.
	 */
	var _receiverId = null;
	var _receiverImsId = null;
	
	
	/**
	 * TellmeRTC의 현재 상태를 저장하는 변수
	 */
	var _tellmeRtcStep = WEBRTC_STEP_NONE;
	
	/**
	 * RTCPeerConnection 호출 후 생성되는 peerconnection을 저장하는 변수
	 */
	var _peerConnection = null;
	
	/**
	 * 
	 */
	var _localDescription;
	
	/**
	 * getUserMedia 결과값 저장
	 */
	var _localStream = null;
	
	/**
	 * local stream을 출력할 audio/video element
	 */
	var _localVideo = null;
	
	/**
	 * remote stream을 출력할 audio/video element
	 */
	var _remoteVideo = null;
	
	
	/**
	 * Test용 id
	 */
	var _callerImsId = "01020998089";
	var _calleeImsId = "01021462946";
	
	/**
	 * WebRTC Signaling에 대해 내부적인 signlaing를 사용할지 설정
	 */
	var _bUseInternalSignaling = false;

	/**
	 * SKT WebRTC의 RCSID
	 */
	var _rcsInstanceId
	
	/**
	 * SKT WebRTC의 서비스 instance url.
	 */
	var _instanceUrl;
	
	/**
	 * SKT WebRTC API 사용 시 인증에 필요한 ouath token
	 */
	var _oauthToken = null;
	
	/**
	 * SKT WebRTC API사용 시 재인증 대기시간
	 */
	var _ouathExpireTime = -1;
	var _bIsReceiveOffer = false;
	var _checkLoop;
	
	var _bIsOnPolling;
	
	/**
	 * 각 peer입장에서 offer/answer가 전달 되어야 pperConnection이 생성되어 candidate를 설정할 수 있다.
	 * 단, 네트워크 환경에 따라 offer/answer 가 전달되기 이전 candidate가 전달될 수 있고, 이를 방지하기 위한 설정값이다.
	 */
	var _msgQueue = [];
	var _bIsSignalingReady = false;
	
	
	/*****************************
	 * 
	 * PUBLIC PROPERTIES
	 * 
	 *****************************/
	/**
	 * TellmeRTC 객체를 초기화하는 메소드
	 * TellmeRTC 사용 전 반드시 호출 되어야 한다.
	 * - Parameter
	 *  role(Number) 				: ROLE_CALLEE 혹은 ROLE_CALLER 값을 설정 
	 *  userId(String) 				: 해당 클라이언트를 고유하게 식별할 수 있는 값
	 *  receiverId(String) 			: 현재 peer와 연결을 시도하는 상대 peer를 식별할 수 있는 값
	 *  rtcStateCallback(Function) 	: TellmeRTC state 변화를 수신 할 callback
	 *  remoteVideo(Object) 		: remote stream을 출력 할 element
	 *  localVideo(Object) 			: local stream을 출력 할 elemet
	 */
	var initialize = function(role, userId, receiverId, rtcStateCallback, remoteVideo, localVideo) {
		dlog("initialize was called - tellmertc.js_v."+ TELLME_RTC_VERSION );
		dlog("user role:"+role);
		
		//TellmeRTC step을 초기화한다.
		_tellmeRtcStep = WEBRTC_STEP_NONE;
		
		//role은 ROLE_CALLEE 혹은 ROLE_CALLER 값 중 하나로 설정되어야 한다.
		if(role != CALLER && role != CALLEE) {
			throw new Error("userRole should be TellmeRTC.ROLE_CALLER or TellmeRTC.ROLE_CALLEE");
			return;
		}
		else
			_userRole = role;
		
		//현재 peer를 식별할 수 있는 id값 
		if(userId == null) {
			throw new Error("userId was not declared");
			return;
		}
		else {
			_userId = userId;
			_imsId = userId.split('#')[1];
		}
		
		if(receiverId != null) {
			_receiverId = receiverId;
			_receiverImsId = receiverId.split('#')[1];
		}
		
		_localVideo = localVideo;
		if(remoteVideo == null){
			throw new Error("remoteElemet was not declared");
			return;
		}
		else 
			_remoteVideo = remoteVideo;
		
		if(rtcStateCallback == null){
			throw new Error("rtcStateCallback was not declared");
			return;
		}
		else
			ontmrtcstateCallback = rtcStateCallback;
	};
	
	/**
	 * WebRTC signaling 과정 시 사용 할 channel 객체를 초기화한다.
	 * TellmeRTC 자체에 channel 객체를 포함하지는 않으며, 외부 channel을 초기화 한 후 다음의 callback를 설정한다.
	 * 만약 initWebRTC 호출 시 useInternalChannel == true인 경우 해당 메소드를 호출하지 않아도 된다.
	 * - Parameter
	 *  onsendcallback(Function) : WebRTC 모듈이 signaling를 할 때 사용 할 channel의 send 메소드를 설정한다.
	 *  onrecvcallback(Function) : WebRTC 모듈이 signaling를 할 때 사용 할 channel의 receive 메소드를 설정한다.
	 */
	var initChannel = function(onsendcallback, onrecvcallback) {
		dlog("initChannel was called");
		
		ontmrtcsendmessage = onsendcallback;
		ontmrtcrecvmessage = onrecvcallback;
		
		return onMessageCallback;
	};
	
	/**
	 * TellmeRTC를 시작하기 전 초기화를 진행하는 함수
	 * getUserMedia를 wrapping하고 있다.
	 * - Paramater
	 *  receiverId(String, Nullable) : initialize에서 receiverId를 설정하지 못했을 경우 다시 값을 설정할 수 있다.
	 *  useInternalChannel(Boolean) : 외부 channel을 사용하지 않고 nabel WebRTC메소드를 사용할지 결정
	 */
	var initWebRTC = function(receiverId, useSKTWebRTC) {
		dlog("initWebRTC was called");
		
		//initWebRTC호출 시 state가 WEBRTC_STEP_NONE가 아닐 때에는 init 과정을 호출하지 않는다.
		if(_tellmeRtcStep != WEBRTC_STEP_NONE){
			throw new Error("TellmeRTC is in progress, current state:"+_tellmeRtcStep);
			return;
		}
		
		//자기 자신의 client에 WEBRTC_STEP_INIT 상태 콜백을 호출한다. 
		triggerRtcStateCallback(WEBRTC_STEP_INIT);
		
		if(receiverId != null) {
			if(TEST) {
				_receiverImsId = receiverId;
			}
			else {
				_receiverId = receiverId;
				_receiverImsId = receiverId.split('#')[1];
			}
		}
		
		_bUseInternalSignaling = useSKTWebRTC;
		
		dlog(">>useInternalChannel:"+useSKTWebRTC);
		if(_bUseInternalSignaling) {
			sktrtcRegistUser();
		}
		
		//만약 SKT RTC를 사용하는 경우, sktrtcRegistUser가 최초로 호출될 등록이 완료되기 전 까지,
		//getUserMedia가 호출이 되어서는 안 된다.
		if(_bUseInternalSignaling) {
			
			var loopObj = setInterval(function() {
				if(_oauthToken != null) {
					getUserMedia(mediaConstraints, onUserMediaSuccess, onUserMediaError);
					clearInterval(loopObj);
				}
			}, 500);
			
		}
		//getUserMedia 함수를 통하여 Device의 audio,video 자원요청을 시도한다. 
		//mediaConstraints = {"audio": true, "video": false}; Tellme에서는 audio만 사용한다. 
		else {
			if(TESTS.ONE_SIDE_COMM) {
				if(_userRole == CALLER)
					getUserMedia(callerMediaConst, onUserMediaSuccess, onUserMediaError);
				else
					getUserMedia(calleeMediaConst, onUserMediaSuccess, onUserMediaError);
			}
			else {
				getUserMedia(mediaConstraints, onUserMediaSuccess, onUserMediaError);
			}
		}
	};
	
	/**
	 * TellmeRTC를 실질적으로 실행하는 함수
	 * rtcStartSignlaing을 wrapping하고 있다.
	 * - Paramater
	 *  userRole(Number, Nullable) : initialize에서 userRole를 설정하지 못했을 경우 다시 값을 설정할 수 있다.
	 */
	var startWebRTC = function(userRole) {
		dlog("startWebRTC was called");
		
		if(userRole != null) {
			_userRole = userRole;
		}
		
		rtcStartSignalling();
	};
	
	/**
	 * TellmeRTC 연결을 종료하는 함수
	 * 생성된 WebRTC 관련 객체를 초기화하며, sendBye가 true일 경우 상대 peer로 bye SDP를 전달한다.
	 * - Paramater
	 *  sendBye(Boolean)				: bye SDP를 상대 peer로 전송할지 설정한다.
	 *  receiverId(String, Nullable) 	: initialize 등에서 상대 peer의 id를 설정하지 못했을 경우 bye SDP전송을 위해 해당 값을 설정할 수 있다.
	 */
	var stopWebRTC = function(sendBye, receiverId) {
		if(_tellmeRtcStep == WEBRTC_STEP_NONE)
			return;
		
		if(_peerConnection){
			_peerConnection.close();
			_peerConnection = null;
		}
		
		if(receiverId != null) {
			_receiverId = receiverId;
		}
		
		//WebRTC bye SDP를 전송한다.
		//SKT WebRTC를 사용할 경우, 해당 서버로 종료 메세지를 전송한다.
		//SKT WebRTC를 사용할 경우, sendBye값에 관계 없이 모두 종료 메세지를 전송한다.
		if(_bUseInternalSignaling) {
			if(sendBye)
				sktrtcFinishCall();
		}
		else {
			if(sendBye)
				rtcSendMessage({type: 'bye'});
		}
		
		_bIsSignalingReady = false;
		
		if(_localStream){
			_localStream.stop();
			_localStream = null;
		}
		
		//자기 자신의 client에 WEBRTC_STEP_DISCONNECTED 상태 콜백을 호출한다. 
		triggerRtcStateCallback(WEBRTC_STEP_DISCONNECTED);
		
		//TellmeRTC의 상태를 초기화한다.
		_tellmeRtcStep = WEBRTC_STEP_NONE;
		
		//_oauthToken = null;
	};
	
	/**
	 * TellmeRTC의 모든 기능을 종료하는 함수
	 * TellmeRTC 기능을 사용하는 페이지를 벗어나는 시점에서 해당 함수를 호출하여 자원 정리를 하여야한다.
	 */
	var finish = function() {
		if(_bUseInternalSignaling) {
			sktrtcUnregistUser();
		}
	};
	
	return {
		ROLE_CALLEE : CALLEE,
		ROLE_CALLER : CALLER,
		WEBRTC_STEP_NONE : WEBRTC_STEP_NONE,
		WEBRTC_STEP_INIT : WEBRTC_STEP_INIT,
		WEBRTC_STEP_GUM_SUCCESS : WEBRTC_STEP_GUM_SUCCESS,
		WEBRTC_STEP_GUM_FAILED : WEBRTC_STEP_GUM_FAILED,
		WEBRTC_STEP_SIGNALING : WEBRTC_STEP_SIGNALING,
		WEBRTC_STEP_CONNECTED : WEBRTC_STEP_CONNECTED,
		WEBRTC_STEP_DISCONNECTED : WEBRTC_STEP_DISCONNECTED,
		initialize : initialize,
		initChannel : initChannel,
		initWebRTC : initWebRTC,
		startWebRTC : startWebRTC,
		stopWebRTC : stopWebRTC,
		finish : finish
 	};
 	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * 
	 *****************************/
	/**
	 * TellmeRTC 상태 변화에 대한 변수르 설정함과 동시에
	 * 이를 callback을 등록한 객체에 전달한다.
	 */
	function triggerRtcStateCallback(step) {
		if(ontmrtcstateCallback != null) {
			ontmrtcstateCallback.apply(null, [step]);
		}
		
		_tellmeRtcStep = step;
	}
	
	function dlog(log) {
		if(DEBUG) {
			console.log(log);
		}
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (SKT WebRTC)
	 *****************************/
	function sktrtcRegistUser() {
		dlog("sktrtcRegistUser was called");
		
		if(_oauthToken != null) {
			dlog("client was already registered!!");
			return;
		}
		
		var requestUrl = SERVER_ADDRESS + "/ID/access_token";
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("POST", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 201) {
                    console.log("[sktrtcRegistUser] response type(200) : " + xhr.responseType);
                    console.log("[sktrtcRegistUser] response text(200) : " + xhr.responseText);
                
                    var response_data = JSON.parse(xhr.responseText);
                    var token = response_data.oauth_token;
                    var expire = response_data.expire_time;
                    var rcs_id = response_data.instance_id;
                    console.log("[sktrtcRegistUser] Token  : " + token);
                    console.log("[sktrtcRegistUser] Expire : " + expire);
                    console.log("[sktrtcRegistUser] RCS ID : " + rcs_id);
                    
                    _oauthToken = token;
                    _ouathExpireTime = expire;
                    _rcsInstanceId = rcs_id;
                    
                    sktrtcLongPolling();
                    
                    //규격 문서에 정의되어 있는 것과 같이
                    //로그인 시 전달받은 expire time의 절반만큼의 시간이 흐르면
                    //재등록 API를 호출한다.
                    setInterval(function() {
                    	
                    	sktrtcRefreshRegistration();
                    	
                    }, (_ouathExpireTime/2)*1000);
                } 
            }
        };
        
        //TODO 정식으로 IMS모듈 적용 시 실제 사용 가능한 ID를 입력하도록 변경 필요
        var imsId = _imsId;
        
        if(TEST) {
	        if(_userRole == CALLER) {
	        	imsId = _callerImsId;
	        	//_receiverImsId = _calleeImsId;
	        }
	        else {
	        	imsId = _calleeImsId;
	        }
        }
        
        var requestData = {};
        requestData.userid = imsId;
        requestData.userpwd = imsId;
        
        //Web RTC API 연동 규격서 - 4.1.1
        var requestBody = {};
        requestBody.data = requestData;
        dlog(">>request body:"+JSON.stringify(requestBody));
    
        xhr.send(JSON.stringify(requestBody));
	}
	
	function sktrtcUnregistUser() {
		dlog("sktrtcUnregistUser was called");
		
		var requestUrl =  SERVER_ADDRESS + "/" + _rcsInstanceId + "?oquth_token=" + _oauthToken;
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("DELETE", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	//TODO 정상 종료 reponse에 대한 동작추가
                } 
            }
        };
        
        xhr.send();
	}
	
	function sktrtcRefreshRegistration() {
		dlog("sktrtcRegistUser was called");
		
		var requestUrl = SERVER_ADDRESS + "/" + _rcsInstanceId;
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("PUT", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 201) {
                	dlog("Refresh Registration succed!");
                } 
            }
        };
        
        //Web RTC API 연동 규격서 - 4.1.2
        var requestBody = {};
        requestBody.oauth_token = _oauthToken;
        dlog(">>request body:"+JSON.stringify(requestBody));
    
        xhr.send(JSON.stringify(requestBody));
	}
	
	function sktrtcLongPolling() {
		dlog("sktrtcLongPolling was called");
		
		if( _bIsOnPolling) {
			dlog("sktrtcLongPolling was already called!");
			return;
		}
			
		
		_bIsOnPolling = true;
		
		var requestUrl = SERVER_ADDRESS + "/" + _rcsInstanceId + "/Notifications/Polling";
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("POST", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	dlog("Message!! - client step : " + _tellmeRtcStep + ", _userRole : " + _userRole);
                    console.log("[sktrtcLongPolling] response type : " + xhr.responseType);
                    console.log("[sktrtcLongPolling] response text : " + xhr.responseText);
                    
                    _bIsOnPolling = false;
                    
                    //만약 reponseText가 빈 문자열일 경우, 별도의 처리를 하지 않는다.
                    if(xhr.responseText == "")
                    	return; 
                    
                    var response_data = JSON.parse(xhr.responseText);
                    
                    if(response_data.data != null)
                    	var response_status = response_data.data.status;
                    else
                    	return;
                    
                	//TODO 서버에서 notification이 전달 되었을 때 각 상황에 맞는 코드를 추가
                	//현재 클라이언트가 CALLER인 경우
                	if(_userRole == CALLER) {
                		//CALLER의 상태와 무관하게 연결이 종료되는 경우(status == ended),
                		//연결 종료 동작을 실행한다.
                		if(response_status == "ended") {
                			stopWebRTC();
                		}
                		//현재 상태가 signaling 단계에서 때 noti를 수신한다면,
                		//CALLEE의 answer를 전달받는 상황이다.
                		else if(response_status == "accepted") {
                			//전달받은 data에서 sdp 정보를 확인한다.
                			var offerSdp = response_data.data.sdp.replace(/\\r/g, '\r').replace(/\\n/g, '\n');;
                			
                			//rtcHandleSDP에서 처리할 수 있도록 JSON 객체를 생성한다.
                			var offerObj = {};
                			offerObj.sdp = offerSdp;
                			offerObj.type = "answer";
                			
                			rtcHandleSDP(offerObj);
                			
                			sktrtcLongPolling();
                		}
                	}
                	//현재 클라이언트가 CALLEE인 경우
                	else if(_userRole == CALLEE) {
                		//CALLER의 상태와 무관하게 연결이 종료되는 경우(status == ended),
                		//연결 종료 동작을 실행한다.
                		if(response_status == "ended") {
                			stopWebRTC();
                		}
                		//현재 상태가 signaling 단계에서 때 noti를 수신한다면,
                		//CALLEE의 answer를 전달받는 상황이다.
                		else if(response_status == "accepted") {
                			var response_data = JSON.parse(xhr.responseText);
                			
                			//현재 서비스에 대한 instance id를 저장한다.
                			_instanceUrl = response_data.instance;
                			
                			//전달받은 data에서 sdp 정보를 확인한다.
                			var offerSdp = response_data.data.sdp.replace(/\\r/g, '\r').replace(/\\n/g, '\n');;
                			
                			//rtcHandleSDP에서 처리할 수 있도록 JSON 객체를 생성한다.
                			var offerObj = {};
                			offerObj.sdp = offerSdp;
                			offerObj.type = "offer";
                			
                			rtcHandleSDP(offerObj);
                			
                			sktrtcLongPolling();
                		}
                	}
                }
                else {
                	dlog("error!!");
                	
                	 _bIsOnPolling = false;
                	
                    console.log("[sktrtcLongPolling] response type : " + xhr.responseType);
                    console.log("[sktrtcLongPolling] response text : " + xhr.responseText);
                }
            }
        };
        
        //Web RTC API 연동 규격서 - 4.2.1
        var requestBody = {};
        requestBody.oauth_token = _oauthToken;
        dlog(">>request body:"+JSON.stringify(requestBody));
        
        xhr.send(JSON.stringify(requestBody));
	}
	
	function sktrtcRequestCall(sdp) {
		dlog("sktrtcRequestCall was called");
		
		sktrtcLongPolling();
		
		var requestUrl = SERVER_ADDRESS + "/" + _rcsInstanceId + "/VideoShare";
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("POST", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	//TODO 서버에서 notification이 전달 되었을 때 각 상황에 맞는 코드를 추가
                	var response_data = JSON.parse(xhr.responseText);
                	_instanceUrl = response_data.instance;
                	
                	 console.log("[sktrtcRequestCall] instanceId:" + _instanceUrl);
                } 
            }
        };
        
        //Web RTC API 연동 규격서 - 4.3.1
        var requestData = {};
        requestData.sdp = sdp;
		requestData.recipient = [_receiverImsId];
        
        var requestBody = {};
        requestBody.oauth_token = _oauthToken;
        requestBody.data = requestData;
        dlog(">>request body:"+JSON.stringify(requestBody));
        
        xhr.send(JSON.stringify(requestBody));
	}
	
	function sktrtcReceiveCall(sdp) {
		dlog("sktrtcReceiveCall was called");
		
		var requestUrl = _instanceUrl.replace(":8076", ":8070");
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("PUT", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
        
        //Web RTC API 연동 규격서 - 4.3.1
        var requestData = {};
        requestData.sdp = sdp;
        
        var requestBody = {};
        requestBody.oauth_token = _oauthToken;
        requestBody.data = requestData;
        dlog(">>request body:"+JSON.stringify(requestBody));
        
        xhr.send(JSON.stringify(requestBody));
	}
	
	function sktrtcFinishCall() {
		dlog("sktrtcFinishCall was called");
		
		var requestUrl = _instanceUrl.replace(":8076", ":8070") + "?oauth_token=" + _oauthToken;
		dlog(">>request url:"+requestUrl);
		
		var xhr = new XMLHttpRequest();
		xhr.open("DELETE", requestUrl);
		xhr.setRequestHeader("Content-Type", "application/json;charset=utf-8");
		xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	//TODO 정상 종료 reponse에 대한 동작추가
                	console.log("sktrtcFinishCall succed!");
                } 
            }
        };
        
        xhr.send();
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (Common channel callback)
	 *****************************/
	/**
	 * WebRTC signaling 메세지를 처리하는 callback.
	 * TellmeRTC를 사용할 때, 해당 callback를 initChannel로 전달받아 각 signaling method의 onmessage callback로 설정해야 한다.
	 * 위와 같이 설정해야 아래와 같은 형식의 JSON 메세지를 WebRTC signaling으로 인식, 연결 과정을 진행할 수 있다.
	 * 
	 * ->SENDER:[sender_id],RECEIVER:[receiver_id],ACTION:SEND,TYPE:RTC,MESSAGE:[signaling_message]
	 * 
	 * 위와 같은 형식의 메세지 이외에는 initChannel의 onrecvcallback을 통해 외부에서 처리할 수 있다.
	 */
	function onMessageCallback(message) {
		var msg = JSON.parse(message);
		
		var argAction = msg.ACTION;
		var argType = msg.TYPE;
		var argMessage = msg.MESSAGE;
		
		//JSON 메세지 상에 TellmeRTC가 처리하여야 하는 메세지가 없는 경우,
		//외부 channel에서 이를 처리하도록 ontmrtcrecvmessage를 호출한다.
		if(argAction == null && argType == null && argMessage == null) {
			ontmrtcrecvmessage.apply(null, [message]);
		}
		else {
			if(argAction == "SEND" && argType == "RTC") {
				rtcQueueMessage(argMessage);
			}
			else {
				ontmrtcrecvmessage.apply(null, [message]);
			}
		}
	}

	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (WebRTC)
	 *****************************/
	/**
	 * remote peer로부터 전달 받은 offer, answer, candidate를 임시로 queing 하는 함수
	 * oofer/answer 이전에 candidate가 전달되어 오류가 발생하는 것을 막기 위함
	 */
	function rtcQueueMessage(message) {
		var msg = JSON.parse(message);
		
		if(msg.type == "offer" || msg.type == "answer") {
			_bIsSignalingReady = true;
			
			rtcHandleSDP(msg);
		}
		else if(msg.type == "candidate") {
			if(_bIsSignalingReady) {
				
				if(_msgQueue.length > 0) {
					rtcHandleSDP(_msgQueue.shift());
				}
				
				rtcHandleSDP(msg);
			}
			else {
				_msgQueue.push(msg);
			}
		}
		else if(msg.type == "bye") {
			rtcHandleSDP(msg);
		}
	}
	
	/**
	 * 전달받은 SDP유형에 따라 WebRTC 연결 과정을 진행하는 함수
	 */
	function rtcHandleSDP(msg) {
		dlog('S->C: ' + JSON.stringify(msg));
		
		//type=offer인 경우,
		//해당 SDP를 remote description으로 설정한 후, 이에 대한 answer를 생성한다.
		if(msg.type == "offer") {
			msg.sdp =  maybePreferAudioSendCodec(msg.sdp);
			_peerConnection.setRemoteDescription(new RTCSessionDescription(msg), onSetSessionRemoteDescriptionSuccess, onSetSessionDescriptionError);
			
			if(TESTS.ONE_SIDE_COMM)
				_peerConnection.createAnswer(setLocalAndSendMessage, onCreateSessionDescriptionError, calleeSdpConstraints);
			else
				_peerConnection.createAnswer(setLocalAndSendMessage, onCreateSessionDescriptionError, sdpConstraints);
			
			_bIsReceiveOffer = true;
			
			return;
		}
		
		//type=answer인 경우,
		//해당 SDP를 remote description으로 설정한다.
		if(msg.type == "answer") {
			msg.sdp = maybePreferAudioSendCodec(msg.sdp);
			
			_peerConnection.setRemoteDescription(new RTCSessionDescription(msg), onSetSessionRemoteDescriptionSuccess, onSetSessionDescriptionError);
			
			return;
		}
		
		//type=candidate인 경우,
		//해당 SDP로 RTCIceCandidate를 생성하여, peerConnection에 candidate를 추가한다.
		if(msg.type == "candidate") {
		    var candidate = new RTCIceCandidate({sdpMLineIndex: msg.label, candidate: msg.candidate});
		    //addIceCandidate는 상대 peer로 부터 cadidate가 수신 되었을때 호출한다. 
		    _peerConnection.addIceCandidate(candidate);
			return;
		}
		
		//type=bye인 경우,
		//WebRTC 연결을 해제한다.
		if(msg.type == "bye") {
			stopWebRTC(false);
		}
	}
	
	/**
	 * getUserMedia가 성공했을 경우, local stream으로 RTCPeerConnection을 생성한다.
	 * 만약 현재 peer가 Caller인 경우, Callee peer에 전달 할 offer SDP를 생성한다.
	 */
	function rtcStartSignalling() {
		dlog("rtcStartSignalling was called");
		
		//현재 TellmeRTC 상태를 WEBRTC_STEP_SIGNALING로 변경한다.
		triggerRtcStateCallback(WEBRTC_STEP_SIGNALING);
		
		//RTCPeerConnection을 생성한다.
		rtcCreatePeerConnection();
		//peerConnection 객체에 local <audio>,<video> 의 스트림을 셋팅한다. 
		_peerConnection.addStream(_localStream);
		
		//현재 peer가 Caller인 경우 offer를 생성한다.
		if(_userRole == CALLER) {
			var constraints = mergeConstraints(offerConstraints, sdpConstraints);
			dlog('Sending offer to peer, with constraints: \n' + '  \'' + JSON.stringify(constraints) + '\'.');
			_peerConnection.createOffer(setLocalAndSendMessage, onCreateSessionDescriptionError, constraints);
		}
	}
	
	/**
	 * RTCPeerConnection을 생성한다.
	 * RTCPeerConnection의 설정값은 tellmeconst.js에 정의되어 있다.
	 */
	function rtcCreatePeerConnection() {
		dlog("rtcCreatePeerConnection was called");
		
		var STUN_caller = JSON.stringify(pcConfigCaller);
		var STUN_callee = JSON.stringify(pcConfigCallee);
		
		//PeerConnection 객체를 생성하기 전에 STUN Server를 logging 한다. 
		dlog('STUN_caller ' + STUN_caller);
		dlog('STUN_callee ' + STUN_callee);
		
		try {
			if(_userRole == CALLER)
				_peerConnection = new RTCPeerConnection(pcConfigCaller, pcConstraints);
			else 
				_peerConnection = new RTCPeerConnection(pcConfigCallee, pcConstraints);
			
			//_peerConnection에 candidate가 만들어졌을때 호출되는 콜백을 셋팅한다. 
			_peerConnection.onicecandidate = onIceCandidate;
		}
		catch (e) {
			dlog('Failed to create PeerConnection, exception: ' + e.message);
			return;
		}
		
		//_peerConnection에 상대 peer의 stream 정보를 세팅하는 콜백 
		_peerConnection.onaddstream = onRemoteStreamAdded;
		_peerConnection.onremovestream = onRemoteStreamRemoved;
		_peerConnection.onsignalingstatechange = onSignalingStateChanged;
		
		//_peerConnection의 ICE 연결 상태에 대한 callback
		_peerConnection.oniceconnectionstatechange = onIceConnectionStateChanged;
	}
	
	/**
	 * 다른 peer로 전달 할 JSON 형식의 signaling 메세지를 생성한다.
	 * 메세지의 형식은 다음과 같다.
	 * 
	 * ->SENDER:[sender_id],RECEIVER:[receiver_id],ACTION:SEND,TYPE:RTC,MESSAGE:[signaling_message]
	 */
	function rtcSendMessage(message) {
		var msgString = JSON.stringify(message);
		dlog('C->S: ' + msgString);
		
		var request = {};
		request.SENDER = _userId;
		request.RECEIVER = _receiverId;
		request.ACTION = "SEND";
		request.TYPE = "RTC";
		request.MESSAGE = msgString;
		
		ontmrtcsendmessage.apply(null,[request]);
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (WebRTC Callback)
	 *****************************/
	
	/**
	 * 상대 RTCPeerConnection의 stream을 알아내었을 때 호출되는 callback
	 */
	function onRemoteStreamAdded(event) {
		dlog("onRemoteStreamAdded");
		
		//client의 특정 <audio>,<video> tag에 상대 peer의 스트림 정보를 attach한다. 
		attachMediaStream(_remoteVideo, event.stream);
	}

	function onRemoteStreamRemoved(event) {
		dlog("onRemoteStreamRemoved");
	}
	
	function onSignalingStateChanged(event) {
		if(_peerConnection != null){
			dlog("onSignalingStateChanged : " + _peerConnection.signalingState);
		}
	}
	
	/**
	 * RTCPeerConnection의 ICE 연결 상태에 대한 callback
	 */
	function onIceConnectionStateChanged(event) {
		if(_peerConnection != null){
			dlog("onIceConnectionStateChanged : " + _peerConnection.iceConnectionState);
			
			//만약 ICE 연결 상태가 'connected'일 경우, 연결이 완료 된것으로 확인할 수 있다.
			if(_peerConnection.iceConnectionState == 'connected') {
				
				if(!TESTS.ONE_SIDE_COMM) {
				//TODO API 추가 개발 시, 해당 부분을 외부에서 선택할 수 있도록 수정 필요
					if(_userRole == CALLEE) {
						//Tellme Service에서 callee는 마이크 기능만 함으로 상대편의 stream을 들을 필요가 없다. 
						_remoteVideo.pause();
					}else {
						//Tellme Service에서 caller는 상대 peer의 크기를 최대한으로 한다. 
						_remoteVideo.volume = 1.0;
					}
				}
				else {
					_remoteVideo.volume = 1.0;
				}
				
				//현재 TellmeRTC가 상대편 peer와 연결이 완료 된 상태로 전환한다.
				//이를 client callback에 전달한다. 
				triggerRtcStateCallback(WEBRTC_STEP_CONNECTED);
			}
		}
		else{
			dlog("_peerConnection is null");
		}
	}
	
	/**
	 * RTCPeerConnection에서 ICE Cnadidate가 생성되었을 때 호출되는 callback
	 */
	function onIceCandidate(event) {
		if(event.candidate) {
			//현재 생성된 local candidate의 type을 logging한다. 
			iceCandidateType(event.candidate.candidate);
			
			//현재까지 생성 된 candidate가 포함 된 SDP를 저장한다.
			if(_bUseInternalSignaling) {
				_localDescription = event.currentTarget.localDescription;
			}
			//SKT WebRTC를 사용하지 않을 경우, candidate가 생성되는대로 상대편 peer에게 전달한다.
			else {
				//HOST 연결만을 원할 경우(TESTS.HOST_CONNECTION == true),
				//상대 peer에게 TYPE이 HOST인 candidate만 전달한다.
				if(TESTS.HOST_CONNECTION) {
					if(event.candidate.candidate.indexOf("typ host") != -1) {
						rtcSendMessage({type: 'candidate',
					        label: event.candidate.sdpMLineIndex,
					        id: event.candidate.sdpMid,
					        candidate: event.candidate.candidate});
					}
				}
				//생성된 ICE candidate를 상대편 peer로 전달한다.
				else {
					rtcSendMessage({type: 'candidate',
				        label: event.candidate.sdpMLineIndex,
				        id: event.candidate.sdpMid,
				        candidate: event.candidate.candidate});
				}
			}
		}
		else {
			console.log('End of candidates, state : ' +  _peerConnection.iceConnectionState);
			
			//생성된 oofer/answer를 SKT WebRTC API를 사용하여 상대편 peer에 전달한다.
			//해당 시점에서 sdp를 전달하는 이유는, ice candidate가 null일 때 candidate 수집이 종료되며,
			//종료되는 시점에서 event.currentTarget.localDescription에 candidate가 모두 포함되어 있기 때문이다.
			if(_bUseInternalSignaling) {
				if(_userRole == CALLER) {
					sktrtcRequestCall(_localDescription.sdp);
				}
				else {
					_checkLoop = setInterval(function(){
							if(_bIsReceiveOffer) {
								
								sktrtcReceiveCall(_localDescription.sdp);
								clearInterval(_checkLoop);
							}
						}, 10*1000);
				}
			}
		}
	}
	
	/**
	 * local candidate가 생성되었을 경우 어떤 Type candidate인지 알아내는 함수 
	 */
	function iceCandidateType(candidateSDP) {
		  //TURN 서버로부터 할당된 transport address.
		  if (candidateSDP.indexOf("typ relay ") >= 0)
			  console.log('LocalCandidateType : ' + 'TURN');
		  //NAT의 external network에 할당된 translated transport address.
		  else if (candidateSDP.indexOf("typ srflx ") >= 0)
			  console.log('LocalCandidateType : ' + 'STUN');
		  //장착된 NIC(Network Interface Card)의 transport address.
		  else if (candidateSDP.indexOf("typ host ") >= 0)
			  console.log('LocalCandidateType : ' + 'HOST');
		  else 
			  console.log('LocalCandidateType : ' + 'UNKNOWN');
		}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (WebRTC / setRemoteDescription, setLocalDescription Callback)
	 *****************************/
	function onSetSessionLocalDescriptionSuccess() {
		dlog('Set session Localdescription success.');
	}
	
	function onSetSessionRemoteDescriptionSuccess() {
		dlog('Set session Remotedescription success.');
	}

	function onSetSessionDescriptionError(error) {
		dlog('Failed to set session description: ' + error.toString());
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (WebRTC / createOffer, createAnswer Callback)
	 *****************************/
	/**
	 * createOffer, createAnswer 호출 시 생성된 SDP를 자신의 SDP로 설정
	 */
	function setLocalAndSendMessage(sessionDescription) {
		//[20131224][jphong] 만약 네이블 API를 사용하며 answer SDP를 만드는 클라이언트(callee)의 경우,
		//생성된 SDP에 a=ice-options:google-ice 값을 추가하여야 한다.
		if(_bUseInternalSignaling && _userRole == CALLEE) {
			sessionDescription.sdp += "a=ice-options:google-ice\r\n";
		}
		
		sessionDescription.sdp = maybePreferAudioReceiveCodec(sessionDescription.sdp);
		_peerConnection.setLocalDescription(sessionDescription, onSetSessionLocalDescriptionSuccess, onSetSessionDescriptionError);
		
		//SKT WebRTC를 사용하지 않을 경우,
		//생성된 offer및 answer를 상대편 peer로 전달한다.
		if(!_bUseInternalSignaling) 
			rtcSendMessage(sessionDescription);
	}
	
	function onCreateSessionDescriptionError(error) {
		console.log('Failed to create session description: ' + error.toString());
	}
	
	/*****************************
	 * 
	 * PRIVATE PROPERTIES
	 * (WebRTC / getUserMedia Callback)
	 *****************************/
	function onUserMediaSuccess(stream) {
		_localStream = stream;
		
		attachMediaStream(_localVideo, stream);
		
		//getUserMedia가 정상 동작 했음을 알린다.
		triggerRtcStateCallback(WEBRTC_STEP_GUM_SUCCESS);
	}
	
	function onUserMediaError(error) {
		//getUserMedia 중 오류가 발생 했음을 알린다.
		triggerRtcStateCallback(WEBRTC_STEP_GUM_FAILED);
		
	 	dlog('Failed to get access to local media. Error code was ' +error.code);
	 	
	 	stopWebRTC(true);
	}

	/*****************************
	 * 
	 * From main.js on apprtc.appspot.com
	 * 
	 *****************************/
	function mergeConstraints(cons1, cons2) {
		var merged = cons1;
		for (var name in cons2.mandatory) {
			merged.mandatory[name] = cons2.mandatory[name];
		}
		merged.optional.concat(cons2.optional);
		return merged;
	}

	function maybePreferAudioSendCodec(sdp) {
	  if (audio_send_codec == '') {
	    dlog('No preference on audio send codec.');
	    return sdp;
	  }
	  dlog('Prefer audio send codec: ' + audio_send_codec);
	  return preferAudioCodec(sdp, audio_send_codec);
	}

	function maybePreferAudioReceiveCodec(sdp) {
		if (audio_receive_codec == '') {
			console.log('No preference on audio receive codec.');
			return sdp;
		}
		dlog('Prefer audio receive codec: ' + audio_receive_codec);
		return preferAudioCodec(sdp, audio_receive_codec);
	}

	// Set |codec| as the default audio codec if it's present.
	// The format of |codec| is 'NAME/RATE', e.g. 'opus/48000'.
	function preferAudioCodec(sdp, codec) {
		var fields = codec.split('/');
		if (fields.length != 2) {
			dlog('Invalid codec setting: ' + codec);
			return sdp;
		}
		var name = fields[0];
		var rate = fields[1];
		var sdpLines = sdp.split('\r\n');

		// Search for m line.
		for (var i = 0; i < sdpLines.length; i++) {
			if (sdpLines[i].search('m=audio') !== -1) {
				var mLineIndex = i;
				break;
			}
		}
		if (mLineIndex === null)
			return sdp;

		// If the codec is available, set it as the default in m line.
		for (var i = 0; i < sdpLines.length; i++) {
			if (sdpLines[i].search(name + '/' + rate) !== -1) {
				var regexp = new RegExp(':(\\d+) ' + name + '\\/' + rate, 'i');
				var payload = extractSdp(sdpLines[i], regexp);
				if (payload)
					sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex],
	                                               payload);
			}
			/*
			else if(sdpLines[i].search("a=fmtp") !== -1) {
				var bitrateLine = sdpLines[i];
				var values = bitrateLine + " maxaveragebitrate=6000";
				
				sdpLines[i] = values;
			}
			*/
			else if(sdpLines[i].search("a=maxptime") !== -1) {
				sdpLines[i] = "a=maxptime:3";
				break;
			}
		}

		// Remove CN in m line and sdp.
		sdpLines = removeCN(sdpLines, mLineIndex);

		sdp = sdpLines.join('\r\n');
		return sdp;
	}

	//Set the selected codec to the first in m line.
	function setDefaultCodec(mLine, payload) {
		var elements = mLine.split(' ');
		var newLine = new Array();
		var index = 0;
		for (var i = 0; i < elements.length; i++) {
			if (index === 3) // Format of media starts from the fourth.
				newLine[index++] = payload; // Put target payload to the first.
			if (elements[i] !== payload)
				newLine[index++] = elements[i];
		 }
		return newLine.join(' ');
	}

	//Strip CN from sdp before CN constraints is ready.
	function removeCN(sdpLines, mLineIndex) {
		var mLineElements = sdpLines[mLineIndex].split(' ');
		// Scan from end for the convenience of removing an item.
		for (var i = sdpLines.length-1; i >= 0; i--) {
			var payload = extractSdp(sdpLines[i], /a=rtpmap:(\d+) CN\/\d+/i);
			if (payload) {
				var cnPos = mLineElements.indexOf(payload);
				if (cnPos !== -1) {
					// Remove CN payload from m line.
					mLineElements.splice(cnPos, 1);
				}
				// Remove CN line in sdp
				sdpLines.splice(i, 1);
			}
		}

		sdpLines[mLineIndex] = mLineElements.join(' ');
		return sdpLines;
	}

	function extractSdp(sdpLine, pattern) {
		var result = sdpLine.match(pattern);
		return (result && result.length == 2)? result[1]: null;
	}
}